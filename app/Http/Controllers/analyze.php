<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Analyze extends Controller
{
    public function analyze()
    {
        //vím, že to neni moc laravelovské řešení...
        //ale byl jsem už tak zoufalý, že jsem přemýšlel i nad přenosem dat pomocí cookies :) 
        $url = $_GET['url'];

        $header=get_headers($url);
        
         $statusCode = explode(' ', $header[0]);
         foreach ($header as $prvek)
         {
            //požité
            if( strpos( $prvek, 'Content-Encoding' ) !== false) 
            {
                $contentEncoding = str_replace('Content-Encoding', 'Použité kódování', $prvek);
            }
            else 
            {
                $contentEncoding = 'Server tuto hlavičku nevrací';
            } 
            //podporované
            if( strpos( $prvek, 'Acept-Encoding' ) !== false) 
            {
                $aceptEncoding = $prvek;
            }
            else 
            {
                $aceptEncoding = 'Server tuto hlavičku nevrací';
            }             
            //X-Robots-Tag:
            if( strpos( $prvek, 'X-Robots-Tag:' ) !== false) 
            {
                $XRobotsTag = $prvek;
            }
            else 
            {
                $XRobotsTag = 'Server tuto hlavičku nevrací';
            }  
           
            //echo $prvek.'<hr>';
        }
        if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false ) 
        {
            $webPSupport = 'podporováno';
        }
        else
        {
            $webPSupport = 'nepodporováno';    
        }
        if($statusCode[1]==200)
        {
            $tags = get_meta_tags($url);

            if(isset($tags['robots']))
            $robots = $tags['robots'];
        else
            $robots = 'hlavička neni vyplněna';
        }
        else 
        {
            $robots = 'Nepodařilo se zjistit';        
        }
            


        if (file_exists((parse_url($url)['scheme'].'://'.parse_url($url)['host'].'/robots.txt')))
        {
            $robotsUrl = parse_url($url)['scheme'].'://'.parse_url($url)['host'].'/robots.txt';
            $file = fopen($robotsUrl,"r");

            while(! feof($file))
            {
                //řádek bez mezer a lomítek
                $radek = str_replace(array('/', ' '), '', fgets($file));
                $radek = trim(preg_replace('/\s+/', ' ', $radek));
                //zkontroluju si Allow, protože má vyšší hodnotu, než disallow
                if(strpos($radek, 'Allow' ) !== false) 
                {
                    //odstraním vše, aby zbyla jen povolená složka
                    $radek = str_replace('Allow:', '', $radek);
                    //Je třeba ověřit, jestli obsahuje hodnotu, jinak může házet error
                    if($radek<>'')
                    {
                        //porovnám s URL
                        if(strpos($url, $radek) == true) 
                        {
                            $robotsTxt = 1;
                        }
                        //zkontroluju si, jestli obsahuje disallow 
                        elseif(strpos($radek, 'Disallow' ) !== false)
                        {
                            //odstraním vše, aby zbyla jen zakázaná složka
                            $radek = str_replace('Disallow:', '', $radek);
                            //porovnám s URL
                                if(strpos($url, $radek) == true) 
                                {
                                    $robotsTxt = 0;
                                }
                        } 
                    }
                } 
                        
            }
            fclose($file);        
            
            if((isset($robotsTxt)) AND ($robotsTxt==0))
                $robotsTxt = 'Přístup pro vyhledávače je blokovaný!';
            else
                $robotsTxt = 'Přístup pro vyhledávače je povolený!';      
        }
        else 
        {
            $robotsTxt = 'Soubor robots.txt neni na svém místě nebo neexistuje';    
        }


        $this->alt_jedna=array();
        $this->alt_dva=array();
        $this->alt_tri=array();
        if($statusCode[1]==200)
        {
            $str = file_get_contents($url);
            $match = preg_match_all('/\<(.*?)\>/',$str,$title); 
            foreach ($title[0] as &$value) 
            {
                //zkontroluju, jestli jde o obrázek
                if((strpos($value, 'img') == true) AND (strpos($value, 'src') == true))
                {
                    //obsauje řetězec alt?
                    if(strpos($value, 'alt') == true)
                    {
                        //je alt vyplněný?
                        if((strpos(str_replace(" ", "", $value), 'alt=""') == true) OR ((strpos(str_replace(" ", "", $value), "alt=''") == true)))
                        {
                                //Alt v obrázku je prázdný
                                array_push($this->alt_jedna, htmlspecialchars ($value));
                        }
                        else 
                        {
                                //v pořádku
                                array_push($this->alt_dva, htmlspecialchars ($value));                      
                        }
                    }
                    else
                    {
                        //Tento obrázek neobsahuje alt:
                        array_push($this->alt_tri, htmlspecialchars ($value));
                    }

                }
            }
        }
        else 
        {
            array_push($this->alt_jedna, 'Nepodařilo se zjistit');
            array_push($this->alt_dva, 'Nepodařilo se zjistit');
            array_push($this->alt_tri, 'Nepodařilo se zjistit');   
        }

        
        $url = $_GET['url'];
        $content=file_get_contents("https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=".$url."&key=AIzaSyASHSwXYkYH9AlzDUT5E62eJy1Xmhsr2ME");
        $google_api = json_decode($content, true);


        //připravím si data pro view
        $data = array("google_apis"=>$google_api,"alt_jedna"=>$this->alt_jedna,"alt_dva"=>$this->alt_dva,"alt_tri"=>$this->alt_tri,"robotstxt"=>$robotsTxt, "meta_robots"=>$robots, "protokol"=>$statusCode[0], "status_code"=>$statusCode[1], "content_encoding"=>$contentEncoding, "acept_encoding"=>$aceptEncoding, "xrobotstag"=>$XRobotsTag, "webpsupport"=>$webPSupport);
        //vrátím view
        return view('result', compact('data'));
        
    }

}
