      <table class="table table-striped mt-5">
        <thead>
          <tr class="thead-dark">
            <th scope="col">Parametr</th>
            <th scope="col">Výsledek</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Protokol</td>
            <td>{{$data['protokol']}}</td>
          </tr>
          <tr>
            <td>Status kód</td>
            <td>{{$data['status_code']}}</td>
          </tr>
          <tr>
            <td>Podporovaná komprese dat</td>
            <td>{{$data['acept_encoding']}}</td>
          </tr>
          <tr>
            <td>Použitá komprese dat</td>
            <td>{{$data['content_encoding']}}</td>
          </tr>
          <tr>
            <td class="text-center font-weight-bold" colspan="3">Indexování ve vyhledávačích</td>
          </tr>
          <tr>
            <td>X-Robots-Tag</td>
            <td>{{$data['xrobotstag']}}</td>
          </tr>
          <tr>
            <td>Robots.txt</td>
            <td>{{$data['robotstxt']}}</td>
          </tr>
          <tr>
            <td>Meta tag</td>
            <td>{{$data['meta_robots']}}</td>
          </tr>
          <tr>
            <td class="text-center font-weight-bold" colspan="3">Obrázky</td>
          </tr>
          <tr>
            <td>Výčet souborů</td>
            <td>
              <h5>Alt tag je prázdný u těchto obrázků: </h5><br>            
                @if(isset($data['alt_jedna'][0]))
                    <ol>
                      @foreach($data['alt_jedna'] as $key)
                          <li class="text-secondary">{{ htmlspecialchars_decode ($key) }}</li>
                      @endforeach
                    </ol>
                @else
                  <p class="text-secondary ml-4">0 obrázků</p>
                @endif      
              <h5>Alt tag je vyplněný u těchto obrázků: </h5><br> 
                @if(isset($data['alt_dva'][0]))
                    <ol>
                      @foreach($data['alt_dva'] as $key)
                          <li class="text-secondary">{{ htmlspecialchars_decode ($key) }}</li>
                      @endforeach
                    </ol>
                @else
                  <p class="text-secondary ml-4">0 obrázků</p>
                @endif     
              <h5>Tyto obrázky neobsahují alt tagy: </h5><br>
                @if(isset($data['alt_tri'][0]))
                    <ol>
                      @foreach($data['alt_tri'] as $key)
                          <li class="text-secondary">{{ htmlspecialchars_decode ($key) }}</li>
                      @endforeach
                    </ol>
                @else
                  <p class="text-secondary ml-4">0 obrázků</p>
                @endif     
            </td>
          </tr>
          <tr>
            <td>Podpora webP obrázků</td>
            <td>{{$data['webpsupport']}}</td>
          </tr>
          <tr>
            <td class="text-center font-weight-bold" colspan="3">Google Lighthouse</td>
          </tr>
          <tr>
            <td>{{$data['google_apis']['lighthouseResult']['categories']['performance']['title']}}</td>
            <td>{{$data['google_apis']['lighthouseResult']['categories']['performance']['score']}}</td>
          </tr>
          <tr>
            <td>{{$data['google_apis']['lighthouseResult']['categoryGroups']['seo-mobile']['title']}}</td>
            <td>{{$data['google_apis']['lighthouseResult']['categoryGroups']['seo-mobile']['description']}}</td>
          </tr>
          <tr>
            <td>{{$data['google_apis']['lighthouseResult']['categoryGroups']['seo-content']['title']}}</td>
            <td>{{$data['google_apis']['lighthouseResult']['categoryGroups']['seo-content']['description']}}</td>
          </tr>
          <tr>
            <td>{{$data['google_apis']['lighthouseResult']['categoryGroups']['seo-crawl']['title']}}</td>
            <td>{{$data['google_apis']['lighthouseResult']['categoryGroups']['seo-crawl']['description']}}</td>
          </tr>
          <tr>
            <td>Celkový čas načítání (s)</td>
            <td>{{$data['google_apis']['lighthouseResult']['timing']['total']}}</td>
          </tr>


          
         </tbody>
