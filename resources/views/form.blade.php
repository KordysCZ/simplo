<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../resources/css/bootstrap.css">
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="container mt-5">
        <form method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="url">Vložte URL analyzované stránky</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="Url">
            </div> 
        </form>
        <button id="button" class="btn btn-warning w-100">Analyzovat</button>  
        <div id="ajax" class="mt-5">
            <div id="loading" style="display:none;">    
                <div class="spinner-grow text-secondary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>Stránka se analyzuje
            </div>
        </div>
          
    </div>
     <script src="../resources/js/jquery.js"></script> 
     <script src="../resources/js/script.js"></script>

</body>
</html>
