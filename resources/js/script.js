    let stav = 0;
    $("#button").click(function()
    {   
        if(stav==0)
        {
            //pokud je stav 0, stačí loading jen zobrazit
            $("#loading").css("display", "block", "important");
            stav=1;            
        }
        else
        {
            //pokud je stav 0, je třeba obsah loadingu i zobrazit
            $("#ajax").html('<div id="loading"><div class="spinner-grow text-secondary" role="status"><span class="sr-only">Loading...</span></div>Stránka se analyzuje</div>');
            $("#loading").css("display", "block", "important");   
        }

        $("#loading").css("display", "block", "important");
        let url = $("#url").val();
        $.ajax
        (
            'submit/?url=' + url,
            {
                cache:false,
                success: function(data) {
                $("#ajax").html(data);
                }
            }
        );
    }); 